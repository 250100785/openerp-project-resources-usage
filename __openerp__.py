{
    "name": "Project Usage",
    "version": "1.0",
    "depends": ["base","project","stock"],
    "author": "Gin2",
    "category": "Project",
    "description": """
    This module provides some features to project and task which are:
    Materials, consumables and tools usage.
    """,
    "init_xml": [],
    'update_xml': [
                   "views/project_view.xml",
                   "views/task_view.xml"
                   ],
    'demo_xml': [],
    'installable': True,
    'active': False,
#    'certificate': 'certificate',
}