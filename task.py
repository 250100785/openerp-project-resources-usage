from osv import osv
from osv import fields
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
 

class project_task(osv.osv):
    
    _name = 'project.task'
    _inherit = 'project.task'
    _description = 'project.task'
 
    _columns = {
            'consumable_usage_ids':fields.one2many('project.task.usage.consumable', 'task_id', 'Consumable Usages'),
            'component_usage_ids':fields.one2many('project.task.usage.component', 'task_id', 'Component Usages'),
        }
    

project_task()


class project_task_usage_consumable(osv.osv):
    
    def onchange_consumable(self, cr, uid, ids,consumable_id,context=None):
        v = {}
        domain = {}
        if consumable_id:
            consum = self.pool.get('project.consumable').browse(cr,uid,consumable_id)
            product = self.pool.get('product.product').browse(cr,uid,consum.product_id.id)
            v = { 'uom_id': product.uom_id.id, 'name':consum.name }
            domain = {'uom_id':[('category_id','=',product.uom_id.category_id.id)]}
          
        return {'value':v,'domain':domain}    
        
    _name = 'project.task.usage.consumable'
    _description = 'Usages'
    _columns = {
                'name':fields.char('Code', size=64, required=False),
                'date':fields.date('Date', required=True),
                'consumable_id':fields.many2one('project.consumable', 'Consumable', required=True),
                'quantity':fields.float('Qty'),
                'uom_id':fields.many2one('product.uom', 'UOM', required=True),
                'task_id':fields.many2one('project.task', 'Task', required=True),
                }
    _defaults = {  
        'date': fields.date.context_today
        }

project_task_usage_consumable()


class project_task_usage_component(osv.osv):
    
    def onchange_component(self, cr, uid, ids,component_id,context=None):
        v = {}
        domain = {}
        if component_id:
            compo = self.pool.get('project.component').browse(cr,uid,component_id)
            product = self.pool.get('product.product').browse(cr,uid,compo.product_id.id)
            v = { 'uom_id': product.uom_id.id, 'name':compo.name }
            domain = {'uom_id':[('category_id','=',product.uom_id.category_id.id)]}
          
        return {'value':v,'domain':domain}    
        
    _name = 'project.task.usage.component'
    _description = 'Component Usages'
    _columns = {
                'name':fields.char('Code', size=64, required=False),
                'date':fields.date('Date', required=True),
                'component_id':fields.many2one('project.component', 'Component', required=True),
                'quantity':fields.float('Qty'),
                'uom_id':fields.many2one('product.uom', 'UOM', required=True),
                'task_id':fields.many2one('project.task', 'Task', required=True),
                }
    _defaults = {  
        'date': fields.date.context_today
        }

project_task_usage_component()